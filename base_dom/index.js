// querySelector  : luôn luôn trả về 1 giá trị đầu tiên tìm thấy nếu có
document.querySelector(".container .item").style.color = "blue";

// querySelectorAll : trả về danh sách tất cả giá trị phù hợp

var itemList = document.querySelectorAll(".item");
console.log("- itemList", itemList);

// index
// gọi tới phần tử có index = 1
itemList[1].style.color = "red";
