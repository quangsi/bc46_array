// return về điểm từ 1 thẻ tr
function getScoreFromTr(trTag) {
  var tdList = trTag.querySelectorAll("td");
  var score = tdList[3].innerText * 1;
  return score;
}
// return về tên từ 1 thẻ tr
function getNameFromTr(trTag) {
  var tdList = trTag.querySelectorAll("td");
  return tdList[2].innerText;
}
//  danh sách thẻ tr
var trList = document.querySelectorAll("#tblBody tr");
//  tạo array chứa điểm
var tdList = document.querySelectorAll(".td-scores");
var scoreArr = [];
// duyệt danh sách
for (var i = 0; i < tdList.length; i++) {
  var score = tdList[i].innerText * 1;
  scoreArr.push(score);
}

// --- in ra thông tin của sinh viên giỏi nhất : tìm điểm lớn nhất => vị trí => thẻ tr chứa điểm lớn nhất ---

function showSinhVienGioiNhat() {
  var maxScore = scoreArr[0];
  for (var i = 0; i < scoreArr.length; i++) {
    if (scoreArr[i] > maxScore) {
      maxScore = scoreArr[i];
    }
  }
  var indexMaxScore = scoreArr.indexOf(maxScore);

  var trMax = trList[indexMaxScore];

  var score = getScoreFromTr(trMax);
  // hiển thị kết quả lên layout
  document.getElementById("svGioiNhat").innerText = ` 
    ${name}- ${score}
`;

  // array filter js w3
}
showSinhVienGioiNhat();

function showSinhVienYeuNhat() {
  var minScore = scoreArr[0];
  for (var index = 1; index < scoreArr.length; index++) {
    if (scoreArr[index] < minScore) {
      minScore = scoreArr[index];
    }
  }
  var indexMinScore = scoreArr.indexOf(minScore);

  var trMin = trList[indexMinScore];

  var name = getNameFromTr(trMin);
  var score = getScoreFromTr(trMin);
  // hiển thị kết quả lên layout
  document.getElementById("svYeuNhat").innerText = ` 
    ${name}- ${score}`;
}
showSinhVienYeuNhat();

function showSoSinhVienGioi() {
  var count = 0;
  for (var i = 0; i < trList.length; i++) {
    var score = getScoreFromTr(trList[i]);
    if (score > 8) {
      count++;
    }
  }
  document.getElementById("soSVGioi").innerText = count;
}
showSoSinhVienGioi();

function showDanhSachLonHon5() {
  var contentHMTL = "";
  for (var i = trList.length - 1; i >= 0; i--) {
    var score = getScoreFromTr(trList[i]);
    if (score >= 5) {
      var content = ` <p class="text-danger"> ${getNameFromTr(
        trList[i]
      )}  - ${score}  </p>`;
      contentHMTL += content;
    }
  }
  document.getElementById("dsDiemHon5").innerHTML = contentHMTL;
}
showDanhSachLonHon5();

// slection sort

// mycodeschool
