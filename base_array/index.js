var foodList = ["hủ tiếu", "cơm tấm", "bánh mì thịt"];

// array ~ mảng ~ [] ~  dấu phẩy

// phương thức đi kèm array

// lấy độ dài, số lượng phần tử
var tongMon = foodList.length;
console.log(" tongMon", tongMon);

// duyệt và in ra danh sách món ăn
for (var i = 0; i < foodList.length; i++) {
  console.log("món :", foodList[i]);
}

// thêm phần tử vào array
foodList.push("phở");
console.log(foodList);

// update 1 phần tử trong array
foodList[3] = "phở đặc biệt";
console.log(foodList);

// duyệt mảng bằng forEach

foodList.forEach(function (monAn) {
  console.log("item", monAn);
});
// forEach w3
// callback

// crud ~ create , read , update, delete
